//
//  LoginViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 29/07/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit
import LRMobileSDK
//import IHKeyboardAvoiding
import SwiftSpinner
import SCLAlertView
import FacebookLogin
import GoogleSignIn
import MaterialComponents.MaterialTextFields

class LoginViewController: UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource, GIDSignInUIDelegate, GIDSignInDelegate {
    
    var showSignup: Bool! = false
    var labelSignup: UILabel!
    var labelPrivacy: UILabel!
    var buttonSignup: UIButton!
    var textFieldUser: MDCTextField!
    var textFieldPass: MDCTextField!
    var textControllerUser: MDCTextInputControllerUnderline!
    var textControllerPass: MDCTextInputControllerUnderline!
    
    @IBOutlet var facebookSignup: UIButton!
    @IBOutlet var collectionView: UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        facebookSignup.addTarget(self, action: #selector(loginButtonClicked), for: UIControl.Event.touchUpInside)
        
        GIDSignIn.sharedInstance().uiDelegate = self
        GIDSignIn.sharedInstance().delegate = self
        GIDSignIn.sharedInstance().signOut()
        
        self.collectionView.performBatchUpdates(nil, completion: {
            (result) in
            self.loadMaterialComponents()
            if self.showSignup {
                self.scrollToNextItem()
            }
        })
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        let notificationCenter = NotificationCenter.default
        notificationCenter.addObserver(self, selector: #selector(self.adjustForKeyboard(notification:)), name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        let notificationCenter = NotificationCenter.default
        notificationCenter.removeObserver(self, name: UIResponder.keyboardWillChangeFrameNotification, object: nil)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func login() {
        SwiftSpinner.show("Cargando...")
        let session = LRSession(server: LIFERAY_URL,
                                authentication: LRBasicAuthentication(username: textFieldUser.text!, password: textFieldPass.text!))
        session.onSuccess({ (result) in
            
            #if DEBUG
                print("\(result)")
            #endif
            
            let liferayConfig = result as? NSDictionary
            UserDefaults.standard.set(liferayConfig?.object(forKey: "userId"), forKey:"userId")
            //print(UserDefaults.standard.value(forKey: "userId")!)
            //UserDefaults.standard.set("101678", forKey:"userId")
            UserDefaults.standard.set(self.textFieldUser.text, forKey: "user")
            UserDefaults.standard.set(self.textFieldPass.text, forKey: "password")
            
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
            self.present(controller, animated: true, completion: nil)
            
            SwiftSpinner.hide()
        }, onFailure: { (error) in
            print("\(String(describing: error))")
            
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: error.localizedDescription)
            
            SwiftSpinner.hide()
        })
        _ = LRSignIn.signIn(with: session, callback: session.callback!, error:nil)
    }
    
    // MARK: - User Interface Loader
    
    func loadMaterialComponents() {
        textFieldUser = collectionView.viewWithTag(100) as? MDCTextField
        textControllerUser = MDCTextInputControllerUnderline.init(textInput: textFieldUser)
        textControllerUser.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        textFieldPass = collectionView.viewWithTag(200) as? MDCTextField
        textControllerPass = MDCTextInputControllerUnderline.init(textInput: textFieldPass)
        textControllerPass.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        var tap: UITapGestureRecognizer
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        let signupString = NSMutableAttributedString(string: "¡Si no tienes cuenta, regístrate ahora!")
        let range = NSRange(location: 22, length: 17)
        signupString.addAttribute(NSAttributedString.Key.foregroundColor,
                                  value: UIColor(red:0.63, green:0.13, blue:0.34, alpha:1.0),
                                  range: range)
        buttonSignup = collectionView.viewWithTag(300) as? UIButton
        buttonSignup.setAttributedTitle(signupString, for: .normal)
        
        if labelPrivacy != nil {
            let privacyString = NSMutableAttributedString(string: "He leído el Aviso de Privacidad y otorgo mi consentimiento para que los datos personales sean tratados conforme al mismo.")
            let privacyRange = NSRange(location: 12, length: 20)
            privacyString.addAttribute(NSAttributedString.Key.foregroundColor,
                                       value: UIColor(red:0.63, green:0.13, blue:0.34, alpha:1.0),
                                       range: privacyRange)
            labelPrivacy = collectionView.viewWithTag(400) as? UILabel
            labelPrivacy.attributedText = privacyString
        }
    }
    
    // MARK: - Keyboard handling
    @objc func adjustForKeyboard(notification: Notification) {
        
        print("\(notification)")
        
        if let userInfo = notification.userInfo, let endFrame = (userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? NSValue)?.cgRectValue {
            
            if (endFrame.origin.y) >= UIScreen.main.bounds.size.height {
                //self.keyboardHeightLayoutConstraint?.constant = 0.0
                let contentInsets: UIEdgeInsets = .zero
                self.tableView.contentInset = contentInsets
                self.tableView.scrollIndicatorInsets = contentInsets
                print("1")
                print("\(contentInsets)")
                
            } else {
                /*let contentInsets: UIEdgeInsets = UIEdgeInsetsMake(0.0, 0.0, endFrame.size.height-200, 0.0); //05 is padding between your textfield and keypad.
                self.tableView.contentInset = contentInsets
                self.tableView.scrollIndicatorInsets = contentInsets
                print("2")
                print("\(contentInsets)")*/
                let indexPath = IndexPath.init(row: 1, section: 0)
                tableView.scrollToRow(at: indexPath, at: .top, animated: true)
            }
            self.tableView.updateConstraints()
            self.tableView.layoutIfNeeded()
        }
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    // MARK: - Facebook Sign In
    
    @objc func loginButtonClicked() {
        let loginManager = LoginManager()
        loginManager.logIn(readPermissions: [.publicProfile], viewController: self, completion: { loginResult in
            switch loginResult {
            case .failed(let error):
                print(error)
            case .cancelled:
                print("User cancelled login.")
            case .success(let grantedPermissions, let declinedPermissions, let accessToken):
                print(grantedPermissions)
                print(declinedPermissions)
                print(accessToken)
                self.userLogin()
            }
        })
    }
    
    // MARK: - Google Sign In
    
    @IBAction func signInGoogle(_: UIButton) {
        GIDSignIn.sharedInstance().signIn()
    }
    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error!) {
        if let error = error {
            print("\(error.localizedDescription)")
        } else {
            print(user)
            print(user.profile.email)
            self.userLogin()
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        print("\(error.localizedDescription)")
    }
    
    // MARK: - User Login
    
    func userLogin() {
        let storyBoard : UIStoryboard = UIStoryboard(name: "Main", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "SWRevealViewController") as UIViewController
        self.present(nextViewController, animated:true, completion:nil)
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 3
    }
    
    // MARK: - Collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell
        if indexPath.row == 0 {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SigninCell", for: indexPath)
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "SignupCell", for: indexPath)
        }
        return cell
    }
    
    @IBAction func scrollToNextItem() {
        let contentOffset = CGFloat(floor(collectionView.contentOffset.x + collectionView.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(collectionView.contentOffset.x - collectionView.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x: contentOffset, y: collectionView.contentOffset.y , width: collectionView.frame.width, height: collectionView.frame.height)
        collectionView.scrollRectToVisible(frame, animated: true)
    }

}
