//
//  RequestInformationViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 29/07/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit
import Lottie
import STHTTPRequest
import SwiftSpinner
import SCLAlertView
import SWRevealViewController

class RequestInformationViewController: UITableViewController, UISearchBarDelegate {
    
    var animationAdded: Bool = false
    var data: NSMutableArray = []
    var page: Int = 1
    var hideMenu: Bool! = false
    var stringSearch: String = ""
    
    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet var searchBar:UISearchBar!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "Consulta de solicitudes"
        
        UIApplication.shared.statusBarStyle = .lightContent
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        searchBar.delegate = self
        /*let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(self.dismissKeyboard))
        view.addGestureRecognizer(tap)*/
        
        /*let animationView = LOTAnimationView(name: "search")
        animationView.frame = CGRect(x: 0,
                                     y: 0,
                                     width: animationBackground.frame.size.width,
                                     height: animationBackground.frame.size.height)
        animationBackground.addSubview(animationView)
        animationView.loopAnimation = true
        animationView.play{ (finished) in
            
        }*/
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if hideMenu {
            self.navigationItem.setLeftBarButton(nil, animated: true)
            self.navigationController?.navigationBar.barTintColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
            self.navigationController?.navigationBar.isTranslucent = false
            self.navigationController?.navigationBar.tintColor = UIColor.white
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData( search:String, withPage page:Int ) {
        SwiftSpinner.show("Cargando...")
        let r = STHTTPRequest(urlString:"\(WS_URL)/infomex/busqueda/solicitudes")
        r?.postDictionary = ["cadenaBusqueda":"\(search)",
                             "fuenteSolicitud":"1",
                             "tipoBusqueda":"1",
                             "numeroPagina":"\(page)",
                             "tamanioPagina":"20"]
        r?.completionBlock = { (headers, body) in
            let jsonData = body!.data(using: .utf8)
            let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
            let jsonArray = jsonDict?.object(forKey: "listaResultados") as! NSArray
            self.data.addObjects(from: jsonArray as! [Any])
            self.tableView.reloadData()
            SwiftSpinner.hide()
        }
        r?.errorBlock = { (error) in
            print(error!)
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: (error?.localizedDescription)!)
            SwiftSpinner.hide()
        }
        r?.startAsynchronous()
    }
    
    // MARK: - Search bar delegate
    
    /*func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        print(searchBar.text!)
    }*/
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
        data = NSMutableArray.init()
        if (searchBar.text?.count)! > 0 {
            self.stringSearch = searchBar.text!
            self.loadData(search: searchBar.text!, withPage: 1)
        }
    }
    
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if data.count > 0 {
            return data.count + 1
        }
        return 1
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var cell:UITableViewCell!
        if data.count > 0 {
            if indexPath.row < data.count {
                let request = data.object(at: indexPath.row) as! NSDictionary
                cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath)
                
                let title = cell.viewWithTag(1) as! UILabel
                let folio = cell.viewWithTag(2) as! UILabel
                let entity = cell.viewWithTag(4) as! UILabel
                
                if request.object(forKey: "titulo") is NSNull {
                    title.text = ""
                } else {
                    title.text = (request.object(forKey: "titulo") as! String)
                }
                
                if request.object(forKey: "folio") is NSNull {
                    folio.text = ""
                } else {
                    folio.text = (request.object(forKey: "folio") as! String)
                }
                
                if request.object(forKey: "entidad") is NSNull {
                    entity.text = ""
                } else {
                    entity.text = (request.object(forKey: "entidad") as! String)
                }
                
            } else if indexPath.row == data.count {
                cell = tableView.dequeueReusableCell(withIdentifier: "RefreshCell", for: indexPath)
                page = page + 1
                self.loadData(search: "Gobierno", withPage: page)
            }
        } else {
            cell = tableView.dequeueReusableCell(withIdentifier: "PlaceholderCell", for: indexPath)
            if animationAdded == false {
                let animationBackground:UIView = cell.viewWithTag(1)!
                let animationView = LOTAnimationView(name: "search")
                animationView.frame = CGRect(x: 0,
                                             y: 0,
                                             width: animationBackground.frame.size.width,
                                             height: animationBackground.frame.size.height)
                animationBackground.addSubview(animationView)
                animationView.loopAnimation = true
                animationView.play{ (finished) in
                }
                animationAdded = true
            }
        }

        return cell
    }

    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if data.count > 0 {
            return 160
        }
        return 400
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        let requestVC = segue.destination as! RequestDetailViewController
        let button:UIButton = sender as! UIButton
        let buttonPosition:CGPoint = button.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        let request = data.object(at: indexPath!.row) as! NSDictionary
        print(indexPath as Any)
        
        if request.object(forKey: "folio") is NSNull {
        } else {
            requestVC.stringFolio = (request.object(forKey: "folio") as! String)
        }
        
        if request.object(forKey: "coleccion") is NSNull {
        } else {
            let stringCollection:String = request.object(forKey: "coleccion") as! String
            requestVC.stringCollection = stringCollection
            if  stringCollection == "AdjFederal"{
                requestVC.stringCollection = "Federal"
            }
        }
        
        requestVC.stringSearch = self.stringSearch
    }

}
