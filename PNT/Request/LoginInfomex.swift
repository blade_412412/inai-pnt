//
//  LoginInfomex.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 25/09/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import Foundation
import STHTTPRequest

func loginInfomex (entity:String) -> Bool {
    
    let r = STHTTPRequest(urlString:"\(WS_URL)/infomex3/login")
    r?.rawPOSTData = "{\"login\":[{\"usuario\":\"PNT_\(UserDefaults.standard.value(forKey: "userId") ?? "")\",\"password\":\"1nF0m3x2016\",\"idInfomex\":\"\(entity)\"}]}".data(using: .utf8)
    r?.setHeaderWithName("Content-Type", value: "application/json")
    let body:String = try! (r?.startSynchronous())!
    print(body)
    
    if body.count > 0 {
        let jsonData = body.data(using: .utf8)
        let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
        let jsonArray = jsonDict?.object(forKey: "result") as! NSArray
        let session = jsonArray.firstObject as! NSDictionary
        if session.object(forKey: "token") is NSNull {
            return false
        } else {
            UserDefaults.standard.set(session.object(forKey: "token"), forKey:"token_\(entity)")
            return true
        }
    } else {
        return false
    }
    
}

func signupFederalInfomex() -> Bool {
    
    let r = STHTTPRequest(urlString:"\(WS_URL)/infomex/registro")
    r?.rawPOSTData = "{\"codigoPostal\":\"00000\",\"otraOcupacion\":\"\",\"numeroInterior\":\"\",\"idDerechoAcceso\":\"1\",\"apellidoPaterno\":\".\",\"tipoPersonaJuridica\":\"1\",\"idOcupacion\":\"0\",\"fechaNacimiento\":\"01/01/2016\",\"recibirNotificacion\":\"false\",\"idInfomex\":\"gof\",\"ciudadExt\":\"Seleccione DelegaciÃ³n/Municipio\",\"nombre\":\"Prueba\",\"numeroExterior\":\"0\",\"idPais\":\"131\",\"idColonia\":\"-1\",\"apellidoMaterno\":\"\",\"nombreUsuario\":\"PNT_\(UserDefaults.standard.value(forKey: "userId") ?? "")\",\"otroMedio\":\"\",\"idMunicipio\":\"-1\",\"telefono\":\"\",\"correoe\":\"\(UserDefaults.standard.value(forKey: "user") ?? "")\",\"contrasena\":\"1nF0m3x2016\",\"otroNivelEducacion\":\"\",\"estadoExt\":\"\",\"idNivelEducacion\":\"00\",\"idEstado\":\"-1\",\"calle\":\"No proporcionado\",\"recontrasena\":\"1nF0m3x2016\"}".data(using: .utf8)
    r?.setHeaderWithName("Content-Type", value: "application/json")
    let body:String = try! (r?.startSynchronous())!
    print(body)
    
    if body.count > 0 {
        let jsonData = body.data(using: .utf8)
        let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
        let token = jsonDict?.object(forKey: "token") as! String
        if token.count > 0 {
            return true
        } else {
            return false
        }
    } else {
        return false
    }
    
}

func signupStateInfomex(entity:String) -> Bool {
    
    let r = STHTTPRequest(urlString:"\(WS_URL)/infomex_estatal/registro")
    r?.rawPOSTData = "{\"RepLegalApaterno\":\"-\",\"Login\":\"PNT_\(UserDefaults.standard.value(forKey: "userId") ?? "")\",\"FechaUltimaModif\":\"27/09/2018\",\"Activo\":\"1\",\"Password\":\"1nF0m3x2016\",\"Bloqueado\":\"0\",\"respuestasecreta\":\"1nF0m3x2016\",\"RFC\":\"-\",\"idInfomex\":\"\(entity)\",\"RepLegal\":\"-\",\"guidpreguntacontrasena\":\"1\",\"guidusuario\":\"\(UserDefaults.standard.value(forKey: "userId") ?? "")\",\"Apaterno\":\"\",\"Apellidos\":\"\",\"Correo\":\"\(UserDefaults.standard.value(forKey: "user") ?? "")\",\"guididioma\":\"1\",\"Nombre\":\"\",\"Guidpais\":\"131\",\"Persona\":\"FÃ­sica\",\"FNacimiento\":\"27/09/2018\",\"CambiarPass\":\"0\"}".data(using: .utf8)
    r?.setHeaderWithName("Content-Type", value: "application/json")
    let body:String = try! (r?.startSynchronous())!
    print(body)
    
    if body.count > 0 {
        let jsonData = body.data(using: .utf8)
        let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
        if jsonDict?.object(forKey: "Resultado") is NSNull {
            return false
        } else {
            let jsonArray = jsonDict?.object(forKey: "Resultado") as! NSArray
            let session = jsonArray.firstObject as! NSDictionary
            if session.object(forKey: "Guidusuario") is NSNull {
                return false
            } else {
                return true
            }
        }
    } else {
        return false
    }
    
}
