//
//  NewRequestViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 02/08/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit
import SwiftSpinner
import SWRevealViewController
import ActionSheetPicker_3_0
import STHTTPRequest
import SCLAlertView
import MaterialComponents.MaterialTextFields
import DLRadioButton
import MobileCoreServices

class NewRequestViewController: UITableViewController, UIDocumentMenuDelegate,UIDocumentPickerDelegate,UINavigationControllerDelegate, UITextFieldDelegate {

    var base64File:String = ""
    var extFile:String = ""
    var stringState:String = ""
    var indexDelivery:NSInteger = 1
    var indexAccessibility:NSInteger = 0
    var indexesAccessibility:String = ""
    var otherAccessEnabled:Bool = false
    
    var textControllerEntity: MDCTextInputControllerUnderline!
    var textControllerSubject: MDCTextInputControllerUnderline!
    var textControllerDescription: MDCTextInputControllerUnderline!
    var textControllerComplement: MDCTextInputControllerUnderline!
    var textControllerAttachment: MDCTextInputControllerUnderline!
    var textControllerLanguage: MDCTextInputControllerUnderline!
    var textControllerState: MDCTextInputControllerUnderline!
    var textControllerLocality: MDCTextInputControllerUnderline!
    var textControllerOther: MDCTextInputControllerUnderline!
    
    @IBOutlet var textFieldEntity: MDCTextField!
    @IBOutlet var textFieldSubject: MDCTextField!
    @IBOutlet var textFieldDescription: MDCTextField!
    @IBOutlet var textFieldComplement: MDCTextField!
    @IBOutlet var textFieldAttachment: MDCTextField!
    @IBOutlet var textFieldLanguage: MDCTextField!
    @IBOutlet var textFieldState: MDCTextField!
    @IBOutlet var textFieldLocality: MDCTextField!
    @IBOutlet var textFieldOther: MDCTextField!
    @IBOutlet weak var menuButton:UIBarButtonItem!
    
    @IBOutlet var switchDone:UISwitch!
    @IBOutlet var deliveryCollection: [DLRadioButton]!
    @IBOutlet var accessibilityCollection: [DLRadioButton]!
    @IBOutlet var mAccessibilityCollection: [DLRadioButton]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        var tap: UITapGestureRecognizer
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
        textControllerEntity = MDCTextInputControllerUnderline.init(textInput: textFieldEntity)
        textControllerEntity.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerSubject = MDCTextInputControllerUnderline.init(textInput: textFieldSubject)
        textControllerSubject.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerDescription = MDCTextInputControllerUnderline.init(textInput: textFieldDescription)
        textControllerDescription.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerComplement = MDCTextInputControllerUnderline.init(textInput: textFieldComplement)
        textControllerComplement.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerAttachment = MDCTextInputControllerUnderline.init(textInput: textFieldAttachment)
        textControllerAttachment.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerLanguage = MDCTextInputControllerUnderline.init(textInput: textFieldLanguage)
        textControllerLanguage.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerState = MDCTextInputControllerUnderline.init(textInput: textFieldState)
        textControllerState.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerLocality = MDCTextInputControllerUnderline.init(textInput: textFieldLocality)
        textControllerLocality.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerOther = MDCTextInputControllerUnderline.init(textInput: textFieldOther)
        textControllerOther.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        textFieldDescription.delegate = self
        textFieldDescription.placeholder = "Descripción (4000)"
        textFieldComplement.delegate = self
        textFieldComplement.placeholder = "Complemento de descripción (4000)"
        mAccessibilityCollection[0].isMultipleSelectionEnabled = true
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reload),
            name: NSNotification.Name(rawValue: "ReloadSubjects"),
            object: nil)
    }
    
    /*override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        sharedSubjectManager.subjectCollection.removeAllObjects()
    }*/
    
    deinit {
        sharedSubjectManager.subjectCollection.removeAllObjects()
    }
    
    @objc func reload() {
        self.tableView.reloadData()
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showStates(sender: Any) {
        if let path = Bundle.main.path(forResource: "Infomex", ofType: "plist"),
            
            let states = NSDictionary(contentsOfFile: path){
            let stringStates: NSMutableArray = []
            for state in states {
                if state.key as? String != "Gobierno Federal" {
                    stringStates.add(state.key)
                }
            }
            
            let stateNames = stringStates as AnyObject as! [String]
            var sortedArray = stateNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
            sortedArray.insert("Gobierno Federal", at: 0)
            
            ActionSheetStringPicker.show(withTitle: "Estado o Federación", rows: sortedArray, initialSelection: 0, doneBlock: { (picker, indexes, values) in
                
                let state:String = states.object(forKey: values as Any) as! String
                self.stringState = state
                self.textFieldEntity.text = (values as! String)
                
                return
            }, cancel: {
                ActionMultipleStringCancelBlock in return
            }, origin: sender)
        }
    }
    
    @IBAction func showEntities(sender: Any) {
        if stringState.count > 0 {
            SwiftSpinner.show("Cargando...")
            let r = STHTTPRequest(urlString:"\(WS_URL)/infomex3/\(self.stringState)/dependencias")
            r?.completionBlock = { (headers, body) in
                
                //print(body!)
                
                let jsonData = body!.data(using: .utf8)
                let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
                if (jsonDict?.count)! > 0 {
                    let entities = jsonDict?.object(forKey: "catalogo") as! NSArray
                    let stringEntities: NSMutableArray = []
                    
                    for case let entity as NSDictionary in entities {
                        stringEntities.add(entity.object(forKey: "nombre")!)
                    }
                    
                    let storyboard = UIStoryboard(name: "Main", bundle: nil)
                    let controller:SubjectsTableViewController = storyboard.instantiateViewController(withIdentifier: "SubjectsTableViewController") as! SubjectsTableViewController
                    controller.entities = entities
                    controller.data = stringEntities
                    controller.stringState = self.stringState
                    self.navigationController?.pushViewController(controller, animated: true)
                    
                    SwiftSpinner.hide()
                } else {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Cerrar") {
                        alertView.dismiss(animated: true, completion: nil)
                    }
                    alertView.showError("Ups!", subTitle: ("No se encontraron resultados"))
                    SwiftSpinner.hide()
                }
            }
            r?.errorBlock = { (error) in
                print(error!)
                let appearance = SCLAlertView.SCLAppearance(
                    showCloseButton: false
                )
                let alertView = SCLAlertView(appearance: appearance)
                alertView.addButton("Cerrar") {
                    alertView.dismiss(animated: true, completion: nil)
                }
                alertView.showError("Ups!", subTitle: (error?.localizedDescription)!)
                SwiftSpinner.hide()
            }
            r?.startAsynchronous()
        } else {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: "Debes seleccionar primero estado o federación")
        }
    }

    @IBAction func selectDelivery(sender: Any) {
        let deliveryButton:DLRadioButton = sender as! DLRadioButton
        self.indexDelivery = deliveryButton.tag
        //print(self.indexDelivery)
        for case let dButton:DLRadioButton in self.deliveryCollection {
            dButton.isSelected = false
        }
        deliveryButton.isSelected = true
    }
    
    @IBAction func selectAccessibility(sender: Any) {
        let accessibilityButton:DLRadioButton = sender as! DLRadioButton
        self.indexAccessibility = accessibilityButton.tag
        //print(self.indexAccessibility)
        for case let dButton:DLRadioButton in self.accessibilityCollection {
            dButton.isSelected = false
        }
        accessibilityButton.isSelected = true
    }
    
    @IBAction func selectmAccessibility(sender: Any) {
        var index:NSInteger = 1
        let options:NSMutableArray = []
        for case let dButton:DLRadioButton in self.mAccessibilityCollection {
            self.otherAccessEnabled = false
            textFieldOther.isEnabled = false
            if dButton.isSelected == true {
                options.add("\(index)")
                if index == 5 {
                    self.otherAccessEnabled = true
                    textFieldOther.isEnabled = true
                }
            }
            index = index + 1
        }
        var mAccesibility:String = ""
        for option in options {
            mAccesibility += "\(option),"
        }
        if mAccesibility.count > 0 {
             mAccesibility.remove(at: mAccesibility.index(before: mAccesibility.endIndex))
        }
        print(mAccesibility)
        self.indexesAccessibility = mAccesibility
    }
    
    // MARK: - Document picker delegate
    
    @IBAction func getDocumentFromFiles(sender: Any) {
        let importMenu = UIDocumentPickerViewController(documentTypes: [String(kUTTypePDF), String(kUTTypeArchive), String(kUTTypePlainText), String(kUTTypeZipArchive)], in: .import)
        importMenu.delegate = self
        importMenu.modalPresentationStyle = .formSheet
        self.present(importMenu, animated: true, completion: nil)
    }
    
    public func documentPicker(_ controller: UIDocumentPickerViewController, didPickDocumentAt url: URL) {
        let myURL = url as URL
        print("import result : \(myURL)")
        let urlString:NSString = url.absoluteString as NSString
        textFieldAttachment.text = urlString.lastPathComponent
        let fileData = try! Data.init(contentsOf: url)
        let fileStream:String = fileData.base64EncodedString(options: NSData.Base64EncodingOptions.init(rawValue: 0))
        print(fileStream)
        self.base64File = fileStream
        self.extFile = urlString.pathExtension
    }

    func documentMenu(_ documentMenu: UIDocumentMenuViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        documentPicker.delegate = self
        present(documentPicker, animated: true, completion: nil)
    }
    
    func documentPickerWasCancelled(_ controller: UIDocumentPickerViewController) {
        print("view was cancelled")
        controller.dismiss(animated: true, completion: nil)
    }
    
    func json(from object:Any) -> String? {
        guard let data = try? JSONSerialization.data(withJSONObject: object, options: []) else {
            return nil
        }
        return String(data: data, encoding: String.Encoding.utf8)
    }
    
    @IBAction func sendNewRequest(sender: Any) {
        //print(sharedSubjectManager.subjectCollection)
        
        if sharedSubjectManager.subjectCollection.count == 0 {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: "No hay sujetos obligados seleccionados")
            return
        }
        
        if textFieldDescription.text?.count == 0 {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: "No has ingresado la descripción de la solicitud")
            return
        }
        
        if self.switchDone.isOn == false {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: "Debes aceptar el aviso de privacidad para poder enviar tus solicitudes")
            return
        }
        
        SwiftSpinner.show("Cargando...")
        DispatchQueue.global().async {
            
            var successCount = 0
            var errorCount = 0
            var receiverArray = "\"destinatarios\":["
            let keysArray:NSArray = sharedSubjectManager.subjectCollection!.allKeys as NSArray
            let tokenDictionary:NSMutableDictionary = NSMutableDictionary.init()
            
            for key in keysArray {
                
                let subject:NSDictionary = sharedSubjectManager.subjectCollection.object(forKey: key) as! NSDictionary
                
                if tokenDictionary.object(forKey: subject.object(forKey: "state")!) == nil {
                    if loginInfomex(entity: subject.object(forKey: "state")! as! String) {
                        let receiver = ["identificador":"\(subject.object(forKey: "state")!)",
                            "idInfomex":"\(subject.object(forKey: "state")!)",
                            "dependencia":"\(key)",
                            "dependenciaNombre":"\(subject.object(forKey: "name")!)",
                            "guidUsuario":"\(UserDefaults.standard.value(forKey: "userId")!)",
                            "token":"\(UserDefaults.standard.value(forKey: "token_\(subject.object(forKey: "state")! as! String)") ?? "")",
                            "idModEntrega":"5"]
                        receiverArray = receiverArray + self.json(from: receiver)! + ","
                        tokenDictionary.setValue("", forKey: subject.object(forKey: "state") as! String)
                        //print("\(json(from: receiver) ?? "")")
                    } else {
                        if subject.object(forKey: "state")! as! String == "gof" {
                            if signupFederalInfomex() == true {
                                if loginInfomex(entity: subject.object(forKey: "state")! as! String) {
                                    let receiver = ["identificador":"\(subject.object(forKey: "state")!)",
                                        "idInfomex":"\(subject.object(forKey: "state")!)",
                                        "dependencia":"\(key)",
                                        "dependenciaNombre":"\(subject.object(forKey: "name")!)",
                                        "guidUsuario":"\(UserDefaults.standard.value(forKey: "userId")!)",
                                        "token":"\(UserDefaults.standard.value(forKey: "token_\(subject.object(forKey: "state")! as! String)") ?? "")",
                                        "idModEntrega":"5"]
                                    receiverArray = receiverArray + self.json(from: receiver)! + ","
                                    //print("\(json(from: receiver) ?? "")")
                                }
                            }
                        } else {
                            if signupStateInfomex(entity: subject.object(forKey: "state")! as! String) == true {
                                if loginInfomex(entity: subject.object(forKey: "state")! as! String) {
                                    let receiver = ["identificador":"\(subject.object(forKey: "state")!)",
                                        "idInfomex":"\(subject.object(forKey: "state")!)",
                                        "dependencia":"\(key)",
                                        "dependenciaNombre":"\(subject.object(forKey: "name")!)",
                                        "guidUsuario":"\(UserDefaults.standard.value(forKey: "userId")!)",
                                        "token":"\(UserDefaults.standard.value(forKey: "token_\(subject.object(forKey: "state")! as! String)") ?? "")",
                                        "idModEntrega":"5"]
                                    receiverArray = receiverArray + self.json(from: receiver)! + ","
                                    tokenDictionary.setValue("", forKey: subject.object(forKey: "state") as! String)
                                    //print("\(json(from: receiver) ?? "")")
                                } else {
                                    errorCount = errorCount + 1
                                }
                            }
                        }
                    }
                } else {
                    let receiver = ["identificador":"\(subject.object(forKey: "state")!)",
                        "idInfomex":"\(subject.object(forKey: "state")!)",
                        "dependencia":"\(key)",
                        "dependenciaNombre":"\(subject.object(forKey: "name")!)",
                        "guidUsuario":"\(UserDefaults.standard.value(forKey: "userId")!)",
                        "token":"\(UserDefaults.standard.value(forKey: "token_\(subject.object(forKey: "state")! as! String)") ?? "")",
                        "idModEntrega":"5"]
                    receiverArray = receiverArray + self.json(from: receiver)! + ","
                }
                
            }
            if receiverArray.count > 17 {
                receiverArray.remove(at: receiverArray.index(before: receiverArray.endIndex))
            } else {
                SwiftSpinner.hide()
                DispatchQueue.main.sync {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Cerrar") {
                        alertView.dismiss(animated: true, completion: nil)
                    }
                    alertView.showError("Ups!", subTitle: "Ocurrió un problema al procesar tu solicitud")
                }
                return
            }
            receiverArray = receiverArray + "]"
            //print(receiverArray)
            
            var idAccessibility:String = ""
            if self.indexAccessibility != 0 {
                idAccessibility = "\(self.indexAccessibility)"
            }
            
            var otherAccessString = ""
            if self.otherAccessEnabled == true {
                otherAccessString = self.textFieldOther.text!
            }
            
            var dataArray:String = "\"datos\":{\"idTipoSolicitanteFed\":\"1\",\"idTipoSolicitanteEst\":\"Física\",\"tipoSolicitudFed\":\"0\",\"tipoSolicitudEst\":\"d9b93aad-212b-4127-9d78-e86b23327a93\",\"idTipoDerecho\":\"\",\"sexoFed\":\"H\",\"sexoEst\":\"f8fe5f93-2aeb-4a5b-84ce-671070278c09\",\"nombre\":\"\",\"apellidoPaterno\":\"\",\"apellidoMaterno\":\"\",\"nombreRazonSocial\":\"\",\"informacionSolicitada\":\"\(self.textFieldDescription.text ?? "")\",\"descripcion\":\"\(self.textFieldComplement.text ?? "")\",\"domicilioExtranjero\":\"false\",\"pais\":\"131\",\"estado\":\"\",\"municipio\":\"-1\",\"colonia\":\"-1\",\"paisNombre\":\"México\",\"estadoNombre\":\"\",\"municipioNombre\":\"Seleccione Delegación/Municipio\",\"coloniaNombre\":\"Seleccione una Colonia\",\"calle\":\"\",\"codigoPostal\":\"\",\"telefono\":\"0\",\"numeroExt\":\"\",\"numeroInt\":\"\",\"correoElectronico\":\"luis.resendiz@inai.org.mx\",\"otroMedio\":\"Ninguno\",\"representanteLegal\":\"false\",\"nombreRepresentante\":\"Ninguno\",\"fechaNacimiento\":\"01/01/2018\",\"idNivelEducativoFed\":\"00\",\"idNivelEducativoEst\":\"ec869566-d873-4b8c-acfa-b51401e370d8\",\"NivelEducativoEst\":\"Otro\",\"idOcupacionEst\":\"\",\"idOcupacion\":\"0\",\"otroOcupacion\":\"\",\"idAccesoInformacion\":\"1\",\"otroAccesoInformacion\":\"\",\"follint\":\"\",\"lenguaIndigena\":\"\(self.textFieldLanguage.text ?? "")\",\"entidad\":\"\(self.textFieldState.text ?? "")\",\"municipioLocalidad\":\"\(self.textFieldLocality.text ?? "")\",\"idFormatoAcceso\":\"\(idAccessibility)\",\"puebloIndigena\":\"0\",\"nombrePuebloIndigena\":\"\",\"medidasAccesibilidad\":\"\(self.indexesAccessibility)\",\"otraMedidaAcceso\":\"\(otherAccessString)\",\"idMedioRecepcion\":\"1\",\"correoNotificacion\":\"\""
            
            if self.base64File.count > 0 {
                dataArray = dataArray + ",\"archivo\":\"\(self.base64File)\",\"contentType\":\"\(self.extFile)\""
            }
            
            dataArray = dataArray + "}"
            
            let request:String = "{" + receiverArray + "," + dataArray + "}"
            print(request)
            
            let r = STHTTPRequest(urlString:"\(WS_URL)/infomex3/solicitud_informacion2")
            r?.rawPOSTData = request.data(using: .utf8)
            r?.setHeaderWithName("Content-Type", value: "application/json")
            r?.timeoutSeconds = 180
            let body:String = try! (r?.startSynchronous())!
            
            if r?.error != nil {
                SwiftSpinner.hide()
                DispatchQueue.main.sync {
                    let appearance = SCLAlertView.SCLAppearance(
                        showCloseButton: false
                    )
                    let alertView = SCLAlertView(appearance: appearance)
                    alertView.addButton("Cerrar") {
                        alertView.dismiss(animated: true, completion: nil)
                    }
                    alertView.showError("Ups!", subTitle: (r?.error.localizedDescription)!)
                }
            } else {
                if body.count > 0 {
                    
                    print(body)
                    
                    let jsonData = body.data(using: .utf8)
                    let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
                    let jsonArray = jsonDict?.object(forKey: "result") as! NSArray
                    var folioString = ""
                    for case let response as NSDictionary in jsonArray {
                        print(response)
                        if response.object(forKey: "estatusTransaccion") as! NSInteger == 1 {
                            successCount = successCount + 1
                            folioString = folioString + "\n\(response.object(forKey: "folio") ?? "")"
                        } else {
                            errorCount = errorCount + 1
                        }
                    }
                    let message = "Solicitudes exitosas: \(successCount) \n\nFolios:\(folioString) \n\nSolicitudes fallidas:\(errorCount)"
                    
                    SwiftSpinner.hide()
                    DispatchQueue.main.sync {
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Cerrar") {
                            sharedSubjectManager.subjectCollection.removeAllObjects()
                            let storyboard = UIStoryboard(name: "Main", bundle: nil)
                            let controller:RequestsViewController = storyboard.instantiateViewController(withIdentifier: "RequestsViewController") as! RequestsViewController
                            self.navigationController?.pushViewController(controller, animated: true)
                            alertView.dismiss(animated: true, completion: nil)
                        }
                        alertView.showInfo("Solicitud de información pública", subTitle: message)
                    }
                    
                } else {
                    SwiftSpinner.hide()
                    DispatchQueue.main.sync {
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Cerrar") {
                            alertView.dismiss(animated: true, completion: nil)
                        }
                        alertView.showError("Ups!", subTitle: "Ocurrió un problema al procesar tu solicitud")
                    }
                }
            }
        }
        
    }
    
    // MARK: - Text field delegate
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let text = textField.text else { return true }
        let newLength = text.count + string.count - range.length
        if textField.tag == 1 {
            textField.placeholder = "Descripción (\(4000 - newLength))"
        } else {
            textField.placeholder = "Complemento de descripción (\(4000 - newLength))"
        }
        return newLength <= 4000
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 15
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        switch indexPath.row {
        case 0:
            return 70
        case 1:
            return 70
        case 2:
            return 70
        case 3:
            return 166
        case 4:
            return 44
        case 5:
            return CGFloat(sharedSubjectManager.subjectCollection.count * 68)
        case 6:
            return 70
        case 7:
            return 103
        case 8:
            return 70
        case 9:
            return 103
        case 10:
            return 70
        case 11:
            return 600
        case 12:
            return 520
        case 13:
            return 380
        case 14:
            return 180
        default:
            break
        }
        return 400
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
