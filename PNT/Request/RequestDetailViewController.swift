//
//  RequestDetailViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 24/09/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit
import SwiftSpinner
import STHTTPRequest
import SCLAlertView

class RequestDetailViewController: UITableViewController,  URLSessionDownloadDelegate, URLSessionDelegate, UIDocumentInteractionControllerDelegate {
    
    var stringFolio:String!
    var stringSearch:String!
    var stringCollection:String!
    var stringURLFile:String!
    
    var downloadTask: URLSessionDownloadTask!
    var backgroundSession: URLSession!
    var fileName:String!
    var fileExt:String!
    
    @IBOutlet var labelShippingDate:UILabel!
    @IBOutlet var labelDeliveryDate:UILabel!
    @IBOutlet var labelFolio:UILabel!
    @IBOutlet var labelRequestType:UILabel!
    @IBOutlet var labelDescription:UILabel!
    @IBOutlet var labelNotes:UILabel!
    @IBOutlet var labelAnswerDate:UILabel!
    @IBOutlet var labelResponseType:UILabel!
    @IBOutlet var labelResponse:UILabel!
    @IBOutlet var labelAdditionalInfo:UILabel!
    @IBOutlet var labelInstitution:UILabel!
    @IBOutlet var buttonFile:UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.loadData(folio: self.stringFolio, withSearch: self.stringSearch, withCollection: self.stringCollection)
        
    }
    
    func loadData(folio:String, withSearch search:String, withCollection collection:String) {
        SwiftSpinner.show("Cargando...")
        let r = STHTTPRequest(urlString:"\(WS_URL)/infomex/busqueda/solicitudes/folio/\(folio)?cadenaBusqueda=\(search)&coleccion=\(collection)")
        r?.completionBlock = { (headers, body) in
            let jsonData = body!.data(using: .utf8)
            let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
            let requestDict = jsonDict?.object(forKey: "solicitud") as! NSDictionary
            print(requestDict)
            self.labelShippingDate.text = (requestDict.object(forKey: "fecha") as! String)
            self.labelDeliveryDate.text = (requestDict.object(forKey: "fechaOficial") as! String)
            self.labelFolio.text = (requestDict.object(forKey: "folio") as! String)
            self.labelRequestType.text = (requestDict.object(forKey: "tipo") as! String)
            self.labelDescription.text = (requestDict.object(forKey: "descripcion") as! String)
            self.labelNotes.text = (requestDict.object(forKey: "comentarios") as! String)
            self.labelAnswerDate.text = (requestDict.object(forKey: "fechaRespuesta") as! String)
            self.labelResponseType.text = (requestDict.object(forKey: "medioEntrada") as! String)
            self.labelResponse.text = (requestDict.object(forKey: "respuesta") as! String)
            self.labelAdditionalInfo.text = (requestDict.object(forKey: "informacionAdicional") as! String)
            self.labelInstitution.text = (requestDict.object(forKey: "institucion") as! String)
            
            let urlFile = (requestDict.object(forKey: "archivoRespuesta") as! String)
            if urlFile.count > 0 {
                self.buttonFile.isHidden = false
                self.stringURLFile = urlFile
            }
            
            self.tableView.reloadData()
            SwiftSpinner.hide()
        }
        r?.errorBlock = { (error) in
            print(error!)
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: (error?.localizedDescription)!)
            SwiftSpinner.hide()
        }
        r?.startAsynchronous()
    }
    
    @IBAction func downloadAttachmentFile (sender: Any) {
        
        let backgroundSessionConfiguration = URLSessionConfiguration.background(withIdentifier: "backgroundSession")
        backgroundSession = Foundation.URLSession(configuration: backgroundSessionConfiguration, delegate: self, delegateQueue: OperationQueue.main)
        print(self.stringURLFile)
        let url = URL(string: self.stringURLFile)!
        
        self.fileName = url.deletingPathExtension().lastPathComponent
        self.fileExt = NSURL(fileURLWithPath: url.absoluteString).pathExtension
        
        SwiftSpinner.show("Cargando...")
        downloadTask = backgroundSession.downloadTask(with: url)
        downloadTask.resume()
        
    }
    
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didFinishDownloadingTo location: URL){
        
        SwiftSpinner.hide()
        let path = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory, FileManager.SearchPathDomainMask.userDomainMask, true)
        let documentDirectoryPath:String = path[0]
        let fileManager = FileManager()
        let destinationURLForFile = URL(fileURLWithPath: documentDirectoryPath.appendingFormat("/\(self.fileName ?? "").\(self.fileExt ?? "")"))
        
        if fileManager.fileExists(atPath: destinationURLForFile.path){
            showFileWithPath(path: destinationURLForFile.path)
        } else {
            do {
                try fileManager.moveItem(at: location, to: destinationURLForFile)
                showFileWithPath(path: destinationURLForFile.path)
            } catch {
                print("An error occurred while moving file to destination url")
            }
        }
    }
    
    func urlSession(_ session: URLSession,
                    downloadTask: URLSessionDownloadTask,
                    didWriteData bytesWritten: Int64,
                    totalBytesWritten: Int64,
                    totalBytesExpectedToWrite: Int64){
    }
    
    func urlSession(_ session: URLSession,
                    task: URLSessionTask,
                    didCompleteWithError error: Error?){
        SwiftSpinner.hide()
        downloadTask = nil
        if (error != nil) {
            print(error!.localizedDescription)
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: (error?.localizedDescription)!)
        }
    }
    
    func documentInteractionControllerViewControllerForPreview(_ controller: UIDocumentInteractionController) -> UIViewController
    {
        return self
    }
    
    func showFileWithPath(path: String){
        print(path)
        let isFileFound:Bool? = FileManager.default.fileExists(atPath: path)
        if isFileFound == true {
            let viewer = UIDocumentInteractionController(url: URL(fileURLWithPath: path))
            viewer.delegate = self
            viewer.presentPreview(animated: true)
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 7
    }

    /*
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "reuseIdentifier", for: indexPath)

        // Configure the cell...

        return cell
    }
    */

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
