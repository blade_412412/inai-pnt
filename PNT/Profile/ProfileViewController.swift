//
//  ProfileViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 30/07/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit
import DLRadioButton
import SWRevealViewController
import MaterialComponents.MaterialTextFields

class ProfileViewController: UITableViewController, UICollectionViewDelegate, UICollectionViewDataSource {
    
    var indexUserType:NSInteger = 0
    
    var textFieldName: MDCTextField!
    var textFieldLastName: MDCTextField!
    var textFieldMiddleName: MDCTextField!
    var textFieldPhone: MDCTextField!
    var textFieldUser: MDCTextField!
    
    var textControllerName: MDCTextInputControllerUnderline!
    var textControllerLastName: MDCTextInputControllerUnderline!
    var textControllerMiddleName: MDCTextInputControllerUnderline!
    var textControllerPhone: MDCTextInputControllerUnderline!
    var textControllerUser: MDCTextInputControllerUnderline!

    @IBOutlet weak var menuButton:UIBarButtonItem!
    @IBOutlet var collectionView:UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()

        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        var tap: UITapGestureRecognizer
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)

        self.collectionView.performBatchUpdates(nil, completion: {
            (result) in
            self.loadMaterialComponents()
        })
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadMaterialComponents() {
        
        textFieldName = collectionView.viewWithTag(100) as? MDCTextField
        textControllerName = MDCTextInputControllerUnderline.init(textInput: textFieldName)
        textControllerName.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        textFieldLastName = collectionView.viewWithTag(200) as? MDCTextField
        textControllerLastName = MDCTextInputControllerUnderline.init(textInput: textFieldLastName)
        textControllerLastName.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        textFieldMiddleName = collectionView.viewWithTag(300) as? MDCTextField
        textControllerMiddleName = MDCTextInputControllerUnderline.init(textInput: textFieldMiddleName)
        textControllerMiddleName.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        textFieldPhone = collectionView.viewWithTag(400) as? MDCTextField
        textControllerPhone = MDCTextInputControllerUnderline.init(textInput: textFieldPhone)
        textControllerPhone.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        textFieldUser = collectionView.viewWithTag(500) as? MDCTextField
        textControllerUser = MDCTextInputControllerUnderline.init(textInput: textFieldUser)
        textControllerUser.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        var tap: UITapGestureRecognizer
        tap = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
    }
    
    // MARK: - Radio Button
    
    @IBAction func selectUserType(sender: Any) {
        let userTypeButton:DLRadioButton = sender as! DLRadioButton
        self.indexUserType = userTypeButton.tag
        if ( self.indexUserType == 0 ) {
            textFieldLastName.isEnabled = true
            textFieldMiddleName.isEnabled = true
        } else {
            textFieldLastName.text = ""
            textFieldMiddleName.text = ""
            textFieldLastName.isEnabled = false
            textFieldMiddleName.isEnabled = false
        }
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 2
    }

    // MARK: - Collection view data source
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: UICollectionViewCell
        if indexPath.row == 0 {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "PersonalCell", for: indexPath)
        } else {
            cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AddressCell", for: indexPath)
        }
        return cell
    }
    
    @IBAction func scrollToNextItem() {
        let contentOffset = CGFloat(floor(collectionView.contentOffset.x + collectionView.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    @IBAction func scrollToPreviousItem() {
        let contentOffset = CGFloat(floor(collectionView.contentOffset.x - collectionView.bounds.size.width))
        self.moveToFrame(contentOffset: contentOffset)
    }
    
    func moveToFrame(contentOffset : CGFloat) {
        let frame: CGRect = CGRect(x: contentOffset, y: collectionView.contentOffset.y , width: collectionView.frame.width, height: collectionView.frame.height)
        collectionView.scrollRectToVisible(frame, animated: true)
    }

}
