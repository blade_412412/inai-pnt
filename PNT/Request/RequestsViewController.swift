//
//  RequestsViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 30/07/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit
import SwiftSpinner
import SWRevealViewController
import ActionSheetPicker_3_0
import STHTTPRequest
import SCLAlertView
import MaterialComponents.MaterialTextFields

class RequestsViewController: UITableViewController {
    
    var stringState:String = ""
    var arrayResult:NSArray!
    
    var textControllerEntity: MDCTextInputControllerUnderline!
    var textControllerType: MDCTextInputControllerUnderline!
    var textControllerFolio: MDCTextInputControllerUnderline!
    var textControllerDate: MDCTextInputControllerUnderline!
    var textControllerFinish: MDCTextInputControllerUnderline!
    
    @IBOutlet var textFieldEntity: MDCTextField!
    @IBOutlet var textFieldType: MDCTextField!
    @IBOutlet var textFieldFolio: MDCTextField!
    @IBOutlet var textFieldDate: MDCTextField!
    @IBOutlet var textFieldFinish: MDCTextField!
    @IBOutlet weak var menuButton:UIBarButtonItem!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        if self.revealViewController() != nil {
            menuButton.target = self.revealViewController()
            menuButton.action = #selector(SWRevealViewController.revealToggle(_:))
            self.view.addGestureRecognizer(self.revealViewController().panGestureRecognizer())
        }
        
        textControllerEntity = MDCTextInputControllerUnderline.init(textInput: textFieldEntity)
        textControllerEntity.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerType = MDCTextInputControllerUnderline.init(textInput: textFieldType)
        textControllerType.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerFolio = MDCTextInputControllerUnderline.init(textInput: textFieldFolio)
        textControllerFolio.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerDate = MDCTextInputControllerUnderline.init(textInput: textFieldDate)
        textControllerDate.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        textControllerFinish = MDCTextInputControllerUnderline.init(textInput: textFieldFinish)
        textControllerFinish.activeColor = UIColor(red:0.35, green:0.02, blue:0.46, alpha:1.0)
        
        let tap: UITapGestureRecognizer = UITapGestureRecognizer.init(target: self, action: #selector(dismissKeyboard))
        self.view.addGestureRecognizer(tap)
        
    }
    
    @objc func dismissKeyboard() {
        self.view.endEditing(true)
    }
    
    @IBAction func showStates(sender: Any) {
        
        if let path = Bundle.main.path(forResource: "Infomex", ofType: "plist"),
            let states = NSMutableDictionary(contentsOfFile: path) {
        
                let stringStates: NSMutableArray = []
                for state in states {
                    if state.key as? String != "Gobierno Federal" {
                        stringStates.add(state.key)
                    }
                }
            
                let stateNames = stringStates as AnyObject as! [String]
                var sortedArray = stateNames.sorted { $0.localizedCaseInsensitiveCompare($1) == ComparisonResult.orderedAscending }
                sortedArray.insert("Gobierno Federal", at: 0)
            
                ActionSheetStringPicker.show(withTitle: "Estado o Federación", rows: sortedArray, initialSelection: 0, doneBlock: { (picker, indexes, values) in
                    
                    print("values = \(String(describing: values))")
                    print("indexes = \(String(describing: indexes))")
                    print("picker = \(String(describing: picker))")
                    
                    self.textFieldEntity.text = (values as! String)
                    let state:String = states.object(forKey: values as Any) as! String
                    self.stringState = state
                    print(state)
                    
                    return
                }, cancel: {
                    ActionMultipleStringCancelBlock in return
                }, origin: sender)
        }
    }
    
    @IBAction func showRequestTypes(sender: Any) {
        ActionSheetStringPicker.show(withTitle: "Tipo de solicitud", rows: ["Información Pública", "Datos Personales"], initialSelection: 0, doneBlock: { (picker, indexes, values) in
            self.textFieldType.text = (values as! String)
        }, cancel: {
            ActionMultipleStringCancelBlock in return
        }, origin: sender)
    }
    
    @IBAction func showInitialDate(sender: Any) {
        ActionSheetDatePicker.show(withTitle: "Fecha recepción oficial", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: { (picker, indexes, values) in
            
            let initialDate:NSDate = indexes as! NSDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let stringOfDate = dateFormatter.string(from: initialDate as Date)
            self.textFieldDate.text = stringOfDate
            
        }, cancel: { (ActionSheetDatePicker) in return
            
        }, origin: (sender as! UIView))
    }
    
    @IBAction func showFinishDate(sender: Any) {
        ActionSheetDatePicker.show(withTitle: "Hasta", datePickerMode: UIDatePicker.Mode.date, selectedDate: Date(), doneBlock: { (picker, indexes, values) in
            
            let initialDate:NSDate = indexes as! NSDate
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd-MM-yyyy"
            let stringOfDate = dateFormatter.string(from: initialDate as Date)
            self.textFieldFinish.text = stringOfDate
            
        }, cancel: { (ActionSheetDatePicker) in return
            
        }, origin: (sender as! UIView))
    }
    
    @IBAction func searchRequests(sender: Any) {
        if stringState.count > 0 {
            SwiftSpinner.show("Cargando...")
            DispatchQueue.global().async {
                if self.searchData() {
                    SwiftSpinner.hide()
                    DispatchQueue.main.sync {
                        let storyboard = UIStoryboard(name: "Main", bundle: nil)
                        let controller:RequestTableViewController = storyboard.instantiateViewController(withIdentifier: "RequestTableViewController") as! RequestTableViewController
                        controller.data = self.arrayResult
                        controller.entity = self.stringState
                        self.navigationController?.pushViewController(controller, animated: true)
                    }
                } else {
                    SwiftSpinner.hide()
                    DispatchQueue.main.sync {
                        let appearance = SCLAlertView.SCLAppearance(
                            showCloseButton: false
                        )
                        let alertView = SCLAlertView(appearance: appearance)
                        alertView.addButton("Cerrar") {
                            alertView.dismiss(animated: true, completion: nil)
                        }
                        alertView.showError("Ups!", subTitle: "Búsqueda sin resultados\nIngresa otros filtros para una nueva búsqueda.")
                    }
                }
            }
        } else {
            let appearance = SCLAlertView.SCLAppearance(
                showCloseButton: false
            )
            let alertView = SCLAlertView(appearance: appearance)
            alertView.addButton("Cerrar") {
                alertView.dismiss(animated: true, completion: nil)
            }
            alertView.showError("Ups!", subTitle: "Debes seleccionar estado o federación")
            return
        }
    }
    
    func searchData() -> Bool {
        if loginInfomex(entity: stringState) {
            print(UserDefaults.standard.value(forKey: "token_\(stringState)")!)
        } else {
            return false
        }
        let r = STHTTPRequest(urlString:"\(WS_URL)/infomex3/mis_solicitudes2")
        
        r?.rawPOSTData = "{\"datos\":{\"fechaFin\":\"\(self.textFieldFinish.text ?? "")\",\"fechaInicio\":\"\(self.textFieldDate.text ?? "")\",\"fechaRecepcionFin\":\"\",\"fechaRecepcionInicio\":\"\",\"folio\":\"\(self.textFieldFolio.text ?? "")\",\"numPag\":\"1\",\"numresul\":\"25\",\"statusSolicitud\":\"0\",\"textoSolicitud\":\"\",\"tipoFecha\":\"\",\"tipoSolicitud\":\"\(self.textFieldType.text ?? "")\"},\"destinatarios\":[{\"idInfomex\":\"\(stringState)\",\"identificador\":\"Solicitud_\(stringState)\",\"token\":\"\(UserDefaults.standard.value(forKey: "token_\(stringState)") ?? "")\"}]}".data(using: .utf8);
        
        if stringState == "gof" {
            r?.rawPOSTData = "{\"datos\":{\"fechaRecepcionOficialFinal\":\"\(self.textFieldFinish.text ?? "")\",\"fechaRecepcionOficialInicial\":\"\(self.textFieldDate.text ?? "")\",\"folioSolicitud\":\"\(self.textFieldFolio.text ?? "")\",\"idOrdenarPor\":\"2\",\"idTipoSolicitud\":\"\",\"numPag\":\"1\",\"numResultado\":\"25\",\"orden\":\"2\",\"proceso\":\"1\",\"textoSolicitud\":\"\"},\"destinatarios\":[{\"idInfomex\":\"gof\",\"identificador\":\"Solicitud_gof\",\"token\":\"\(UserDefaults.standard.value(forKey: "token_\(stringState)") ?? "")\"}]}".data(using: .utf8);
        }
        
        r?.setHeaderWithName("Content-Type", value: "application/json")
        let body:String = try! (r?.startSynchronous())!
        print(body)
        let jsonData = body.data(using: .utf8)
        let jsonDict = try? JSONSerialization.jsonObject(with: jsonData!, options: .mutableLeaves) as! NSDictionary
        let result:NSArray = jsonDict?.object(forKey: "result") as! NSArray
        arrayResult = result
        
        return true
        /*let jsonArray = jsonDict?.object(forKey: "result") as! NSArray
         print(jsonArray)*/
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return 8
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.row == 5 {
            let topBarHeight = UIApplication.shared.statusBarFrame.height + self.navigationController!.navigationBar.frame.height
            let viewHeight = self.view.frame.height
            let visibleContentHeight = viewHeight - topBarHeight
            let contentHeight = CGFloat(70 * 7)
            let difference = visibleContentHeight - contentHeight
            print("\(difference)")
            if difference > 0 {
                return difference
            }
            return 0
        }
        return 70
    }

}
