//
//  MainViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 30/07/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit

class MainViewController: UITableViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if UserDefaults.standard.value(forKey: "userId") != nil {
            let storyboard = UIStoryboard(name: "Main", bundle: nil)
            let controller = storyboard.instantiateViewController(withIdentifier: "SWRevealViewController")
            self.present(controller, animated: true, completion: nil)
            return
        }
        
        UIApplication.shared.statusBarStyle = .default
        self.navigationController?.navigationBar.barStyle = .default
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.barTintColor = nil
        self.navigationController?.navigationBar.tintColor = UIColor(red:0.63, green:0.13, blue:0.34, alpha:1.0)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 6
    }
    
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if segue.identifier == "News" {
            let newsVC: NewsViewController = segue.destination as! NewsViewController
            newsVC.hideMenu = true
        }
        if segue.identifier == "RequestInformation" {
            let requestInfoVC: RequestInformationViewController = segue.destination as! RequestInformationViewController
            requestInfoVC.hideMenu = true
        }
        if segue.identifier == "Signup" {
            print("Signup")
            let loginVC: LoginViewController = segue.destination as! LoginViewController
            loginVC.showSignup = true
        }
    }

}
