//
//  SubjectsTableViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 26/09/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit

class SubjectsTableViewController: UITableViewController {

    var stringState:String!
    var data:NSArray!
    var entities:NSArray!
    var selectedIndexes:NSMutableArray = NSMutableArray.init()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.title = "Sujetos obligados \(sharedSubjectManager.subjectCollection.count)/33"
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        NotificationCenter.default.post(name: Notification.Name("ReloadSubjects"), object: nil)
    }
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectCell", for: indexPath)

        let entityName:String = data.object(at: indexPath.row) as! String
        let title = cell.viewWithTag(1) as! UILabel
        title.text = entityName
        
        let entity:NSDictionary = entities.object(at: indexPath.row) as! NSDictionary
        if sharedSubjectManager.subjectCollection.object(forKey: entity.object(forKey: "id")!) == nil {
            cell.accessoryType = UITableViewCell.AccessoryType.none
        } else {
            cell.accessoryType = UITableViewCell.AccessoryType.checkmark
        }
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath)
        tableView.deselectRow(at: indexPath, animated: true)
        
        let entity:NSDictionary = entities.object(at: indexPath.row) as! NSDictionary
        if sharedSubjectManager.subjectCollection.object(forKey: entity.object(forKey: "id")!) == nil {
            if sharedSubjectManager.subjectCollection.count < 33 {
                sharedSubjectManager.subjectCollection.setObject(["state":self.stringState, "name":entity.object(forKey: "nombre")], forKey: entity.object(forKey: "id") as! NSCopying)
                cell!.accessoryType = UITableViewCell.AccessoryType.checkmark
            }
        } else {
            sharedSubjectManager.subjectCollection.removeObject(forKey: entity.object(forKey: "id")!)
            cell!.accessoryType = UITableViewCell.AccessoryType.none
        }
        self.title = "Sujetos obligados \(sharedSubjectManager.subjectCollection.count)/33"
        //print(sharedSubjectManager.subjectCollection)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
