//
//  SubjectsViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 26/09/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit

class SubjectsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    var data:NSArray = []
    var keys:NSArray = []
    @IBOutlet var tableView:UITableView!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NotificationCenter.default.addObserver(
            self,
            selector: #selector(reload),
            name: NSNotification.Name(rawValue: "ReloadSubjects"),
            object: nil)
    }
    
    @objc func reload() {
        data = sharedSubjectManager.subjectCollection.allValues as NSArray
        keys = sharedSubjectManager.subjectCollection.allKeys as NSArray
        self.tableView.reloadData()
    }
    
    @IBAction func deleteSubject(sender: Any) {
        let button:UIButton = sender as! UIButton
        let buttonPosition:CGPoint = button.convert(CGPoint.zero, to:self.tableView)
        let indexPath = self.tableView.indexPathForRow(at: buttonPosition)
        
        let dictKey = keys.object(at: (indexPath?.row)!)
        sharedSubjectManager.subjectCollection.removeObject(forKey: dictKey)
        NotificationCenter.default.post(name: Notification.Name("ReloadSubjects"), object: nil)
        
        print(indexPath!)
    }

    // MARK: - Table view data source

    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return data.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SubjectCell", for: indexPath)
        
        let subject:NSDictionary = data.object(at: indexPath.row) as! NSDictionary
        let title = cell.viewWithTag(1) as! UILabel
        title.text = (subject.object(forKey: "name") as! String)
        
        return cell
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
