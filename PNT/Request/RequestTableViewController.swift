//
//  RequestTableViewController.swift
//  PNT
//
//  Created by Vladimir Rojas Jimenez on 25/09/18.
//  Copyright © 2018 INAI. All rights reserved.
//

import UIKit

class RequestTableViewController: UITableViewController {

    var data:NSArray!
    var entity:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        print(self.data)
        // Uncomment the following line to preserve selection between presentations
        // self.clearsSelectionOnViewWillAppear = false

        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
        // self.navigationItem.rightBarButtonItem = self.editButtonItem
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return data.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "RequestCell", for: indexPath)

        let request = data.object(at: indexPath.row) as! NSDictionary
        
        let title = cell.viewWithTag(1) as! UILabel
        let folio = cell.viewWithTag(2) as! UILabel
        let type = cell.viewWithTag(3) as! UILabel
        
        if request.object(forKey: "dependencia") is NSNull {
            title.text = ""
        } else {
            title.text = (request.object(forKey: "dependencia") as! String)
        }
        
        if request.object(forKey: "folio") is NSNull {
            folio.text = ""
        } else {
            folio.text = (request.object(forKey: "folio") as! String)
        }
        
        if request.object(forKey: "tipoSolicitud") is NSNull {
            type.text = ""
        } else {
            type.text = (request.object(forKey: "tipoSolicitud") as! String)
        }

        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let request = data.object(at: indexPath.row) as! NSDictionary
        print(request)
        let storyboard = UIStoryboard(name: "Main", bundle: nil)
        let controller:PersonalRequestDetailViewController = storyboard.instantiateViewController(withIdentifier: "PersonalRequestDetailViewController") as! PersonalRequestDetailViewController
        controller.stringToken = (UserDefaults.standard.value(forKey: "token_\(self.entity ?? "")") as! String)
        controller.stringFolio = (request.object(forKey: "folio") as! String)
        controller.stringEntity = self.entity
        self.navigationController?.pushViewController(controller, animated: true)
    }

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
